Triangles

Software corresponding to the following research paper:
Marcelo Cicconet, Davi Geiger, Kristin Gunsalus, and Michael Werman.
Mirror Symmetry Histograms for Capturing Geometric Properties in Images.
IEEE Conference on Computer Vision and Pattern Recognition. Columbus, Ohio. 2014.

For usage examples, see

ex_circles.m   (to fit circles of a particular radius),
ex_symaxis.m   (to find the axis of symmetry),
ex_skeleton.m  (to find the medial axis),
ex_junctions.m (to find perpendicular junctions),
ex_eft.m       (to fit ellipses).

To open all .m files, run OpenAll

These functions call routines from the project PAT, available at
https://bitbucket.org/cicconet/pat

To add PAT to the Matlab path:
File > Set Path... > Add Folder... > PAT > Open > Save > Close
