% Copyright � 2014 New York University.
% See notice at the end of this file.

% --------------------------------------------------
% load image, set some parameters
% --------------------------------------------------
Image = imread('C3.png');
Image = double(Image)/255; % input should be double and in the range [0,1]

nestcir = 3; % estimated maximum number of circles

% --------------------------------------------------
% points, tangents, and magnitudes
% --------------------------------------------------
nangs = 32;
stretch = 1;
scale = 1;
hopsize = 5;
halfwindowsize = 1;
magthreshold = 0.01;
ignoredirections = 1;
[m,a,x,y] = coefficientslist(ignoredirections,Image,nangs,stretch,scale,hopsize,halfwindowsize,magthreshold);
% uncomment the following to see the wavelet outputs
% magfactor = 2;
% showoriginal = 0;
% showcoefficients = 1;
% J = drawoutputslist(ignoredirections,Image,x,y,m,a,hopsize,magfactor,showoriginal,showcoefficients);
% imshow(J)
% return

% --------------------------------------------------
% triangles
% --------------------------------------------------
disp('Listing triangles...')
tic
ts = triangleslist(m,a,x,y,@force_cir,size(Image,2),size(Image,1));
toc

% --------------------------------------------------
% projection
% --------------------------------------------------
p4 = [1 0 0 0]; % wmp, mp, wpp, pp
accwidth = size(Image,2);
accheight = size(Image,1);
disp('Projecting...')
tic
A = project2(ts,@location_cir,@force_cir,accwidth,accheight,size(Image,2),size(Image,1),p4);
toc
% uncomment the following to see the accumulator image
% imshow(A)
% return

% --------------------------------------------------
% locate centers
% --------------------------------------------------
hsize = 10;
halfwindow = 10;
mindistbetcent = 10;
lowerbound = 0.5;
minarea = 0.1;
[centers,I,J] = localmaxima(A,hsize,halfwindow,lowerbound,minarea,mindistbetcent,nestcir);
% uncomment the following to see blobs for local maxima location
% imshow([Image A I J])
% return

% --------------------------------------------------
% paint circles
% --------------------------------------------------
radius = 50;
K = repmat(0.25*Image,[1 1 3]);
for i = 1:size(centers,2)
    r = centers(1,i);
    c = centers(2,i);
    npoints = 75;
    as = linspace(0,2*pi,npoints);
    x = round(r+radius*cos(as));
    y = round(c+radius*sin(as));
    for j = 1:npoints
        K(x(j),y(j),1) = 1;
        K(x(j),y(j),2) = 1;
        K(x(j),y(j),3) = 0;
    end
end
imshow(K)

% Copyright � 2014 New York University.
% 
% All Rights Reserved. A license to use and copy this software and its documentation
% solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
% is hereby granted upon your download of the software, through which you agree to the following:
% 1) the above copyright notice, this paragraph and the following paragraphs
% will prominently appear in all internal copies;
% 2) no rights to sublicense or further distribute this software are granted;
% 3) no rights to modify this software are granted; and
% 4) no rights to assign this license are granted.
% Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
% New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
% or for further distribution, modification or license rights.
%  
% Created by Marcelo Cicconet.
%  
% IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (?COLLECTIVELY ?NYU PARTIES?)
% BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
% INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
% EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
%  
% NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
% INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
% AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
% OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
% IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
% NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
% 
% Please cite the following reference if you use this software in your research:
% 
% Marcelo Cicconet, Davi Geiger, Kristin Gunsalus, and Michael Werman.
% Mirror Symmetry Histograms for Capturing Geometric Properties in Images.
% IEEE Conference on Computer Vision and Pattern Recognition. Columbus, Ohio. 2014.