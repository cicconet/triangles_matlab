% Copyright � 2014 New York University.
% See notice at the end of this file.

% --------------------------------------------------
% load image, set some parameters
% --------------------------------------------------
Image = imread('E3.png');
Image = double(Image)/255; % input should be double and in the range [0,1]

nestell = 3; % estimated maximum number of ellipses
radrange = [20 60]; % the estimated semi-minor and semi-major axis should be in this interval

% --------------------------------------------------
% points, tangents, and magnitudes
% --------------------------------------------------
nangs = 32;
stretch = 1;
scale = 1;
hopsize = 5;
halfwindowsize = 1;
magthreshold = 0.01;
ignoredirections = 1;
[m,a,x,y] = coefficientslist(ignoredirections,Image,nangs,stretch,scale,hopsize,halfwindowsize,magthreshold);
% uncomment the following to see the wavelet outputs
% magfactor = 2;
% showoriginal = 0;
% showcoefficients = 1;
% J = drawoutputslist(ignoredirections,Image,x,y,m,a,hopsize,magfactor,showoriginal,showcoefficients);
% imshow(J)
% return

% --------------------------------------------------
% accumulator to locate centers
% --------------------------------------------------
nquadsfactor = 8; % number of pairs of triangles used: nquadsfactor*npairs
mindist = 0.25*radrange(1);
maxdist = 2*radrange(2);
[A,pairs,iquads] = intacc(m,a,x,y,size(Image,1),size(Image,2),nquadsfactor,mindist,maxdist,radrange);
% uncomment the following to see the accumulator image
% imshow(A)
% return

% --------------------------------------------------
% locate centers
% --------------------------------------------------
hsize = 20;
halfwindow = 10;
mindistbetcent = 10;
lowerbound = 0.25;
minarea = 0.1;
[centers,I,J] = localmaxima(A,hsize,halfwindow,lowerbound,minarea,mindistbetcent,nestell);
% uncomment the following to see blobs for local maxima location
% imshow([Image A I J])
% return

% --------------------------------------------------
% separate ellipses
% --------------------------------------------------
proximitythreshold = 2;
ellipseindices = clusterbyellipse(pairs,iquads,centers,proximitythreshold);
% uncomment the following to see boundary points of a particular ellipse
% eli = 3; % ellipse index
% M = pointsinellipseofindex(Image,ellipseindices,eli,iquads,pairs,m,x,y);
% imshow(M)
% return

% --------------------------------------------------
% for every ellipse, find remaining parameters
% --------------------------------------------------
[K,L,parameters] = paintellipses(Image,m,a,x,y,centers,pairs,iquads,ellipseindices);
Output = [repmat([Image A J],[1 1 3]) L K];
imshow(Output)

% Copyright � 2014 New York University.
% 
% All Rights Reserved. A license to use and copy this software and its documentation
% solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
% is hereby granted upon your download of the software, through which you agree to the following:
% 1) the above copyright notice, this paragraph and the following paragraphs
% will prominently appear in all internal copies;
% 2) no rights to sublicense or further distribute this software are granted;
% 3) no rights to modify this software are granted; and
% 4) no rights to assign this license are granted.
% Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
% New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
% or for further distribution, modification or license rights.
%  
% Created by Marcelo Cicconet.
%  
% IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (?COLLECTIVELY ?NYU PARTIES?)
% BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
% INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
% EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
%  
% NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
% INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
% AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
% OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
% IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
% NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
% 
% Please cite the following reference if you use this software in your research:
% 
% Marcelo Cicconet, Davi Geiger, Kristin Gunsalus, and Michael Werman.
% Mirror Symmetry Histograms for Capturing Geometric Properties in Images.
% IEEE Conference on Computer Vision and Pattern Recognition. Columbus, Ohio. 2014.