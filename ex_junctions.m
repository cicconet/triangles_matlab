% Copyright � 2014 New York University.
% See notice at the end of this file.

% --------------------------------------------------
% load image, set some parameters
% --------------------------------------------------
Image = imread('J.png');
Image = double(Image)/255; % input should be double and in the range [0,1]
% imshow(Image)
% imdistline
% return

% --------------------------------------------------
% points, tangents, and magnitudes
% --------------------------------------------------
nangs = 32;
stretch = 1;
scale = 1;
hopsize = 3;
halfwindowsize = 1;
magthreshold = 0.02;
ignoredirections = 0;
[m,a,x,y] = coefficientslist(ignoredirections,Image,nangs,stretch,scale,hopsize,halfwindowsize,magthreshold);
% uncomment the following to see the wavelet outputs
% magfactor = 3;
% showoriginal = 1;
% showcoefficients = 0;
% J = drawoutputslist(ignoredirections,Image,x,y,m,a,hopsize,magfactor,showoriginal,showcoefficients);
% imshow(J)
% return

% --------------------------------------------------
% triangles
% --------------------------------------------------
disp('Listing triangles...')
tic
ts = triangleslist(m,a,x,y,@force_jun,size(Image,2),size(Image,1));
toc

% --------------------------------------------------
% projection
% --------------------------------------------------
p4 = [1 0 0 0]; % wmp, mp, wpp, pp
accwidth = size(Image,2);
accheight = size(Image,1);
disp('Projecting...')
tic
A = project2(ts,@location_jun,@force_jun,accwidth,accheight,size(Image,2),size(Image,1),p4);
toc
% uncomment the following to see the accumulator image
% A = A/max(max(A));
% imshow(A)
% return

% --------------------------------------------------
% finding local maxima in accumulator
% --------------------------------------------------
hsize = 20;
halfwindow = 10;
mindistbetcent = 10;
lowerbound = 0.3;
minarea = 0.1;
nlocmax = Inf;
[locs,Out1,Out2] = localmaxima(A,hsize,halfwindow,lowerbound,minarea,mindistbetcent,nlocmax);
% imshow([Out1 Out2])
% return

% --------------------------------------------------
% show results
% --------------------------------------------------
I = 0.25*Image;
for i = 1:size(locs,2)
    for j = -1:1
        for k = -1:1
            I(locs(1,i)+j,locs(2,i)+k) = 1;
        end
    end
end
imshow(I)

% Copyright � 2014 New York University.
% 
% All Rights Reserved. A license to use and copy this software and its documentation
% solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
% is hereby granted upon your download of the software, through which you agree to the following:
% 1) the above copyright notice, this paragraph and the following paragraphs
% will prominently appear in all internal copies;
% 2) no rights to sublicense or further distribute this software are granted;
% 3) no rights to modify this software are granted; and
% 4) no rights to assign this license are granted.
% Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
% New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
% or for further distribution, modification or license rights.
%  
% Created by Marcelo Cicconet.
%  
% IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (?COLLECTIVELY ?NYU PARTIES?)
% BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
% INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
% EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
%  
% NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
% INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
% AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
% OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
% IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
% NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
% 
% Please cite the following reference if you use this software in your research:
% 
% Marcelo Cicconet, Davi Geiger, Kristin Gunsalus, and Michael Werman.
% Mirror Symmetry Histograms for Capturing Geometric Properties in Images.
% IEEE Conference on Computer Vision and Pattern Recognition. Columbus, Ohio. 2014.