% Copyright � 2014 New York University.
% See notice at the end of this file.

% --------------------------------------------------
% pre-processing
% --------------------------------------------------
Image = imread('S2.jpg');

resizingfactor = 200/max(size(Image)); % max dimension will be 200 after resizing
Image = imresize(Image,resizingfactor);
Image = double(rgb2gray(Image))/255;

Image = adapthisteq(Image,'NumTiles',[8 8]);

% --------------------------------------------------
% compute wavelet coefficients
% --------------------------------------------------
nangs = 32;
stretch = 1;
scale = 2;
hopsize = 5;
halfwindowsize = 1;
magthreshold = 0.01;
ignoredirections = 1;
% J = waveletinimage(I,stretch,scale);
% imshow(J)
% return
[m,a,x,y] = coefficientslist(ignoredirections,Image,nangs,stretch,scale,hopsize,halfwindowsize,magthreshold);
% magfactor = 2;
% showoriginal = 1;
% showcoefficients = 0;
% J = drawoutputslist(ignoredirections,I,x,y,m,a,hopsize,magfactor,showoriginal,showcoefficients);
% imshow(J)
% return

% --------------------------------------------------
% triangles
% --------------------------------------------------
disp('Listing triangles...')
tic
ts = triangleslist(m,a,x,y,@force_sym,size(Image,2),size(Image,1));
toc

% --------------------------------------------------
% projection
% --------------------------------------------------
m = 0.5+0.5*m;
p4 = [1 0 0 0]; % wmp, mp, wpp, pp
accwidth = 2*ceil(sqrt(size(Image,1)^2+size(Image,2)^2))+1; % displacement
accheight = 360; % angle
disp('Projecting...')
tic
A = project2(ts,@location_sym,@force_sym,accwidth,accheight,size(Image,2),size(Image,1),p4);
toc
% uncomment the following to see the accumulator image
% imshow(A)
% return

% --------------------------------------------------
% finding local maxima in accumulator
% --------------------------------------------------
hsize = 20;
halfwindow = 10;
mindistbetcent = 10;
lowerbound = 0.9;
minarea = 0.1;
nlocmax = 1; % just looking for the main symmetry axis
[locs,Out1,Out2] = localmaxima(A,hsize,halfwindow,lowerbound,minarea,mindistbetcent,nlocmax);
% imshow([Out1 Out2])
% return

% --------------------------------------------------
% draw results
% --------------------------------------------------
[G,J] = paintlines(Out1,Image,accwidth,accheight,locs);
G = imresize(G,[size(J,1) size(J,2)]);
imshow([G J])

% Copyright � 2014 New York University.
% 
% All Rights Reserved. A license to use and copy this software and its documentation
% solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
% is hereby granted upon your download of the software, through which you agree to the following:
% 1) the above copyright notice, this paragraph and the following paragraphs
% will prominently appear in all internal copies;
% 2) no rights to sublicense or further distribute this software are granted;
% 3) no rights to modify this software are granted; and
% 4) no rights to assign this license are granted.
% Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
% New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
% or for further distribution, modification or license rights.
%  
% Created by Marcelo Cicconet.
%  
% IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (?COLLECTIVELY ?NYU PARTIES?)
% BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
% INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
% EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
%  
% NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
% INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
% AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
% OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
% IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
% NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
% 
% Please cite the following reference if you use this software in your research:
% 
% Marcelo Cicconet, Davi Geiger, Kristin Gunsalus, and Michael Werman.
% Mirror Symmetry Histograms for Capturing Geometric Properties in Images.
% IEEE Conference on Computer Vision and Pattern Recognition. Columbus, Ohio. 2014.