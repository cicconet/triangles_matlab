% Copyright � 2014 New York University.
% See notice at the end of this file.

function [G,J] = paintlines(Acc,I,accwidth,accheight,locs)

rows = locs(1,:);
cols = locs(2,:);

nlines = length(rows);

G = repmat(Acc,[1 1 3]);
J = repmat(0.5*I,[1 1 3]);
for i = 1:nlines
    for j = -5:5
        for k = -5:5
            row = rows(i)+j;
            col = cols(i)+k;
            if row > 0 && row <= size(G,1) && col > 0 && col <= size(G,2)
                G(row,col,1) = 1;
                G(row,col,2) = 0;
                G(row,col,3) = 0;
            end
        end
    end
    
    gamma = (rows(i)-1)/accheight*pi;
    displacement = cols(i)-(accwidth-1)/2;

    if gamma <= pi/2 % displacement always >= 0
        v = [cos(gamma) sin(gamma)];
        p = displacement*v;
        d = 0;
        vp = [-v(2) v(1)];
        q = round(p+d*vp);
        while q(1) >= 1 && q(2) <= size(I,2)
            if q(1) <= size(I,1) && q(2) >= 1
                J(q(1),q(2),1) = 0;
                J(q(1),q(2),2) = 1;
                J(q(1),q(2),3) = 0;
            end
            d = d+1;
            q = round(p+d*vp);
        end
        d = 0;
        q = round(p-d*vp);
        while q(1) <= size(I,1) && q(2) >= 1
            if q(1) >= 1 && q(2) <= size(I,2)
                J(q(1),q(2),1) = 0;
                J(q(1),q(2),2) = 1;
                J(q(1),q(2),3) = 0;
            end
            d = d+1;
            q = round(p-d*vp);
        end
    else
        v = [cos(gamma) sin(gamma)];
        p = displacement*v;
        d = 0;
        vp = [v(2) -v(1)];
        q = round(p+d*vp);
        while q(1) <= size(I,1) && q(2) <= size(I,2)
            if q(1) >= 1 && q(2) >= 1
                J(q(1),q(2),1) = 0;
                J(q(1),q(2),2) = 0;
                J(q(1),q(2),3) = 1;
            end
            d = d+1;
            q = round(p+d*vp);
        end
    end
end

end

% Copyright � 2014 New York University.
% 
% All Rights Reserved. A license to use and copy this software and its documentation
% solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
% is hereby granted upon your download of the software, through which you agree to the following:
% 1) the above copyright notice, this paragraph and the following paragraphs
% will prominently appear in all internal copies;
% 2) no rights to sublicense or further distribute this software are granted;
% 3) no rights to modify this software are granted; and
% 4) no rights to assign this license are granted.
% Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
% New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
% or for further distribution, modification or license rights.
%  
% Created by Marcelo Cicconet.
%  
% IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (?COLLECTIVELY ?NYU PARTIES?)
% BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
% INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
% EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
%  
% NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
% INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
% AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
% OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
% IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
% NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
% 
% Please cite the following reference if you use this software in your research:
% 
% Marcelo Cicconet, Davi Geiger, Kristin Gunsalus, and Michael Werman.
% Mirror Symmetry Histograms for Capturing Geometric Properties in Images.
% IEEE Conference on Computer Vision and Pattern Recognition. Columbus, Ohio. 2014.