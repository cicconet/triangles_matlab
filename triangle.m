% Copyright � 2014 New York University.
% See notice at the end of this file.

function t = triangle(px,py,pa,pm,qx,qy,qa,qm)

p = [px; py];
q = [qx; qy];
taup = [cos(pa); sin(pa)];
tauq = [cos(qa); sin(qa)];

[intp,denom] = intertanlin(p,q,taup,tauq);
if denom ~= 0
    degeneration = 0; % to be corrected soon
    x = intp;
else
    degeneration = 1; % means third vertex is at infinity
    x = [Inf; Inf];
end

base = norm(q-p);
d = [0; 0];
m = (p+q)/2;
if degeneration == 0
    d = m-x;
    n = norm(d);
    if n > 0.0
        d = d/n;
    end
    pn = (p-x)/norm(p-x);
    qn = (q-x)/norm(q-x);
    degeneration = abs(dot(pn,qn));
end


T_pq = (q-p)/base;
T_perp_pq = [0 -1; 1 0]*T_pq;

theta = ang(T_perp_pq(1),T_perp_pq(2));
S = [cos(2*theta) sin(2*theta); sin(2*theta) -cos(2*theta)]; % reflection matrix

wmp = abs(tauq'*S*taup); % weak mirror potential (the closer to 1, the more mirror symmetric)
mp = 0.5*(1+tauq'*S*taup); % mirror potential
wpp = abs(tauq'*taup); % weak parallel potential (the closer to 1, the more parallel)
pp = 0.5*(1+tauq'*taup); % parallel potential

weight = pm*qm;

t = struct('p',p,'q',q,'taup',taup,'tauq',tauq,...
    'm',m,'x',x,'d',d,'T_pq',T_pq,'T_perp_pq',T_perp_pq,'base',base,...
    'degeneration',degeneration,'wmp',wmp,'mp',mp,'wpp',wpp,'pp',pp,'weight',weight);

end

% Copyright � 2014 New York University.
% 
% All Rights Reserved. A license to use and copy this software and its documentation
% solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
% is hereby granted upon your download of the software, through which you agree to the following:
% 1) the above copyright notice, this paragraph and the following paragraphs
% will prominently appear in all internal copies;
% 2) no rights to sublicense or further distribute this software are granted;
% 3) no rights to modify this software are granted; and
% 4) no rights to assign this license are granted.
% Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
% New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
% or for further distribution, modification or license rights.
%  
% Created by Marcelo Cicconet.
%  
% IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (?COLLECTIVELY ?NYU PARTIES?)
% BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
% INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
% EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
%  
% NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
% INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
% AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
% OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
% IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
% NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
% 
% Please cite the following reference if you use this software in your research:
% 
% Marcelo Cicconet, Davi Geiger, Kristin Gunsalus, and Michael Werman.
% Mirror Symmetry Histograms for Capturing Geometric Properties in Images.
% IEEE Conference on Computer Vision and Pattern Recognition. Columbus, Ohio. 2014.